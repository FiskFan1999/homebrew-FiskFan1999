# homebrew-FiskFan1999

homebrew tap for William Rehwinkel's projects.

```bash
brew tap FiskFan1999/FiskFan1999 https://codeberg.org/FiskFan1999/homebrew-FiskFan1999.git
# available formulas:
brew install peertube-multipart-upload
```
