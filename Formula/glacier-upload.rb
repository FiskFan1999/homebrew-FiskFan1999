class GlacierUpload < Formula
  desc "Upload files to aws glacier"
  homepage "https://codeberg.org/FiskFan1999/glacier-upload/"
  url "https://codeberg.org/FiskFan1999/glacier-upload/archive/v0.0.0.tar.gz"
  sha256 "545c106a3d7259951ee44648b2d7d5e1030a5bf877887682e51d04649132897f"
  license "MIT"

  depends_on "go" => :build

  def install
    system "go", "build", *std_go_args(ldflags: "-s -w")
  end

  test do
    system "false"
  end
end