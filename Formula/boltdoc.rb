class Boltdoc < Formula
  desc "TUI application to inspect and manipulate bolt databases."
  homepage "https://codeberg.org/FiskFan1999/boltdoc"
  url "https://codeberg.org/FiskFan1999/boltdoc/archive/v0.0.0.tar.gz"
  version "v0.0.0"
  sha256 "f6b92d9c426996d67668920912620a53eb86622f4d1d0094cb2cc6ad65169521"
  license "GPL-3.0-or-later"

  depends_on "go" => :build

  def install
    system "go", "build", *std_go_args(ldflags: "-s -w")
  end

  test do
    system "go" "test"
  end
end
