class PeertubeMultipartUpload < Formula
  desc "Upload a video to a Peertube instance via a multipart upload using the REST API"
  homepage "https://codeberg.org/FiskFan1999/peertube-multipart-upload/src/branch/master/README.md"
  url "https://codeberg.org/FiskFan1999/peertube-multipart-upload/archive/v0.0.0.tar.gz"
  sha256 "17bf5eab853efa90f2d3868a6468a5b3eea1f5238ed48457c7b95ef96d020afb"
  license "MIT"
  head "https://codeberg.org/FiskFan1999/peertube-multipart-upload.git", branch: "master"

  depends_on "go" => :build

  def install
    system "go", "build", *std_go_args(ldflags: "-s -w")
  end

  test do
    system "false"
  end
end
